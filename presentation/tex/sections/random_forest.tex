% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Random Forest}

\begin{itemize}
    \item Consists of multiple \textbf{elementary decision trees}.
    \item \textbf{It combines} the simplicity and flexibility of decision trees $\to$ \textbf{significantly improves accuracy}.
    \item Steps to build a random forest:
    \begin{enumerate}[<+->]
        \item Compose a so-called bootstrapped dataset.
        \item Create a decision tree using the newly generated bootstrapped dataset. During creation, only consider a subset of attributes.
        \item Repeat the process until the requires number of trees (in the forest) is created.
    \end{enumerate}
\end{itemize}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Bootstrapping}
\begin{itemize}
    \item Creation of so-called \engname{boostrapped dataset} involves selection of $N$ random rows from the original dataset with size of $N$ rows.
    \item Rows are randomly selected with replacement allowed.
    \item Under the assumption that a specific row may be selected with a probability given by uniform probability distribution, then we may expect that the resulting dataset will contain $\approx 63$\% unique rows.
\end{itemize}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Bootstrapping - Demonstration}

\scriptsize{
    \begin{table}[htbp]
        \centering
        \begin{tabular}{|l|r|r|l|r|}
            \toprule
            \tblcolname{Gender} &
            \tblcolname{Age} &
            \tblcolname{Pclass} &
            \tblcolname{Embarked} &
            \tblcolname{Survived}\\
            \midrule
            male &   22.0 & 3 & S & 0\\
            \hline
            female & 38.0 & 1 & C & 1\\
            \hline
            female & 26.0 & 3 & S & 1 \\
            \hline
            female & 35.0 & 1 & S & 1 \\
            \hline
            male &   35.0 & 3 & S & 0 \\
        \bottomrule
        \end{tabular}
        
        \caption{Original dataset (\datasetname{Titanic}).}
        \label{tab:TitanicDatasetOriginal}
    \end{table}
}

\scriptsize{
    \begin{table}[htbp]
        \centering
        \begin{tabular}{|l|r|r|l|r|}
            \toprule
            \tblcolname{Gender} &
            \tblcolname{Age} &
            \tblcolname{Pclass} &
            \tblcolname{Embarked} &
            \tblcolname{Survived}\\
            \midrule
            \hline
            \onslide<2->{female & 38.0 & 1 & C & 1}\\
            \hline
            \onslide<3->{male &   35.0 & 3 & S & 0}\\
            \hline
            \onslide<4->{female & 26.0 & 3 & S & 1}\\
            \hline
            \onslide<5->{male &   22.0 & 3 & S & 0}\\
            \hline
            \onslide<6->{male &   35.0 & 3 & S & 0}\\
        \bottomrule
        \end{tabular}
        
        \caption{New, \engname{bootstrapped}, dataset.}
        \label{tab:TitanicDatasetBootstrapped}
    \end{table}
}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Bootstrapping - Algorithm}

\begin{algorithm}[H]
    \begin{algorithmic}[1]
        \STATE $\mathcal{D} \gets \text{get\_dataset} \left( \right)$
        \STATE $\mathcal{B} \gets \text{create\_empty\_dataset} \left( \right)$
    
        \FOR{$i \gets 1$ to $\text{size} \left( \mathcal{D} \right)$}
            \STATE $p \gets \text{randint} \left( 1, \text{size} \left( \mathcal{D} \right) \right)$
            \STATE $r \gets \text{get\_row} \left( \mathcal{D}, p \right)$
            \STATE $\text{add\_row} \left( \mathcal{B}, r \right)$
        \ENDFOR
        
        \RETURN $\mathcal{B}$
    \end{algorithmic}
    \caption{Algorithm pseudocode: \engname{bootstrapping}.}
    \label{alg:BootstrappingAlgorithm}
\end{algorithm}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Building Random Forest}

\begin{itemize}
    \item Each tree within the random forest is created using a unique bootstrapped dataset as follows:
    \begin{enumerate}[<+->]
        \item For each tree node consider only a random subset of attributes while ignoring the already used ones.
        \item Keep building the tree until the stopping condition is satisfies (depth, all attributes used, homogenity, etc.)
    \end{enumerate}
\end{itemize}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Random Forest - Algorithm}

\begin{algorithm}[H]
    \begin{algorithmic}[1]
        \STATE $\mathcal{D} \gets \text{get\_dataset} \left( \right)$
        \STATE $M \gets \text{create\_random\_forest} \left( s \right)$
        
        \FOR{$i \gets 1$ to $s$}
            \STATE $\mathcal{B} \gets \text{create\_bootstrapped\_dataset} \left( \mathcal{D} \right)$
            
            \STATE $T \gets \text{build\_tree} \left( \mathcal{B}, k \right)$
            \STATE $\text{expand\_random\_forest} \left( M, T \right)$
        \ENDFOR
        
        \RETURN $M$
    \end{algorithmic}
    \caption{Algorithm pseudocode - random forest.}
    \label{alg:RandomForestCreationAlgorithm}
\end{algorithm}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Tree Creation - The Root}

\begin{itemize}
    \item Always consider only a random subset of attributes ($2$ in this example).
\end{itemize}

\begin{columns}
    \begin{column}{0.5\textwidth}
        \metroset{block=fill}
        
        \onslide<1->{
            \begin{block}{Available attributes}
                \engname{Gender}, \engname{Age}, \engname{Pclass}, \engname{Embarked}.
            \end{block}
        }
        \onslide<2->{
            \begin{block}{Selected subset}
                \engname{Age}, \engname{Pclass}.
            \end{block}
        }
        \onslide<3->{
            \begin{block}{Candidate for separation}
                \engname{Pclass}.
            \end{block}
        }
    \end{column}
    \begin{column}{0.5\textwidth}
        \onslide<4->{
            \begin{tikzpicture}[level distance=1.5cm, level 1/.style={sibling distance=3cm}, level 2/.style={sibling distance=1.5cm}]
                \node {\engname{Pclass}}
                    child {
                        node {.....}
                            child {node {.....}}
                            child {node {.....}}
                    }
                    child {
                        node {.....}
                            child {node {.....}}
                            child {node {.....}}
                    };
            \end{tikzpicture}
        }
    \end{column}
\end{columns}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Tree Creation - Second Level (Left Branch)}

\begin{itemize}
    \item Always consider only a random subset of attributes ($2$ in this example).
\end{itemize}

\begin{columns}
    \begin{column}{0.5\textwidth}
        \metroset{block=fill}
        
        \onslide<1->{
            \begin{block}{Available attributes}
                \engname{Gender}, \engname{Age}, \sout{\engname{Pclass}}, \engname{Embarked}.
            \end{block}
        }
        \onslide<2->{
            \begin{block}{Selected subset}
                \engname{Gender}, \engname{Embarked}.
            \end{block}
        }
        \onslide<3->{
            \begin{block}{Candidate for separation}
                \engname{Gender}.
            \end{block}
        }
    \end{column}
    \begin{column}{0.5\textwidth}
        \onslide<4->{
            \begin{tikzpicture}[level distance=1.5cm, level 1/.style={sibling distance=3cm}, level 2/.style={sibling distance=1.5cm}]
                \node {\engname{Pclass}}
                    child {
                        node {\engname{Gender}}
                            child {node {.....}}
                            child {node {.....}}
                    }
                    child {
                        node {.....}
                            child {node {.....}}
                            child {node {.....}}
                    };
            \end{tikzpicture}
        }
    \end{column}
\end{columns}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Tree Creation - Third Level (Left Branch)}

\begin{itemize}
    \item Always consider only a random subset of attributes ($2$ in this example).
\end{itemize}

\begin{columns}
    \begin{column}{0.5\textwidth}
        \metroset{block=fill}
        
        \onslide<1->{
            \begin{block}{Available attributes}
                \sout{\engname{Gender}}, \engname{Age}, \sout{\engname{Pclass}}, \engname{Embarked}.
            \end{block}
        }
        \onslide<2->{
            \begin{block}{Selected subset}
                \engname{Embarked}, \engname{Age}.
            \end{block}
        }
        \onslide<3->{
            \begin{block}{Candidate for separation}
                \engname{Age}.
            \end{block}
        }
    \end{column}
    \begin{column}{0.5\textwidth}
        \onslide<4->{
            \begin{tikzpicture}[level distance=1.5cm, level 1/.style={sibling distance=3cm}, level 2/.style={sibling distance=1.5cm}]
            \node {\engname{Pclass}}
                child {
                    node {\engname{Gender}}
                        child {node {\engname{Age}}}
                        child {node {.....}}
                }
                child {
                    node {.....}
                        child {node {.....}}
                        child {node {.....}}
                };
            \end{tikzpicture}
        }
    \end{column}
\end{columns}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Random Forest Evaluation}

\begin{itemize}
    \item Bootstrapping $\to$ it often happens that some rows are not selected.
    \item Out-of-Bag dataset $\to$ a dataset containing the \textbf{unselected rows} during the bootstrapping phase.
    \item Trees are evaluated using the our-of-bah dataset containing only the rows on which the tree was not trained.
    \item Determining the outcome of prediction:
    \begin{itemize}
        \item \textbf{classification} $\to$ majority vote (i.e., the most often predicted class),
        \item \textbf{regression} $\to$ a mean of predicted values.
    \end{itemize}
\end{itemize}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Random Forest Evaluation - Classification}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=\linewidth]{figures/random_forest/random_forest_voting.png}
\end{figure}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Number of Features Selection During Model Training}

\begin{itemize}
    \item Trees are always trained solely on a subset of features.
    \item \textbf{The number of features} $\to$ iterative approach: test multiple candidates and choose the one that achieves the best accuracy on validation dataset.
    \item Typically, the initial number of features is equal to $\sqrt{N}$, where $N$ is the total number of features. Subsequently, multiple values below and above this baseline are tested.
\end{itemize}

\end{frame}
% --------------------------------------------------------------------------------------------------