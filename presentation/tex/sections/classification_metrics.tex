% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Classification Error}

\begin{itemize}
    \item The \textbf{proportion} of elements within a given region that \textbf{do not belong} to the most \textbf{prevailing class}.
    \item It is \textbf{sensitive} to \textbf{sub-tree size}.
    \item One of the \textbf{simplest methods} for \textbf{error} computation.
    \item Let $p_k$ be the proportion of the $k$-th class within a given region and $K$ be the total number of classes. Then, the \textbf{classification error} is computed as
    \begin{equation}
        E = 1 - \max \cbrackets{p_k \ | \ k = 1, \dots, K}.
    \end{equation}
\end{itemize}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Classification Error - Demonstration}

\begin{columns}
    \begin{column}{0.4\textwidth}
        \onslide<1->{
            \begin{figure}[htbp]
                \centering
                \includegraphics[width=0.8\linewidth]{figures/classification_metrics/sample_data_circles_region_1.pdf}
            \end{figure}
        }
    \end{column}
    \begin{column}{0.6\textwidth}
        \onslide<2->{
            \begin{equation*}
                E = 1 - \max \cbrackets{\frac{8}{8}} = 1 - \frac{8}{8} = 0
            \end{equation*}
        }
    \end{column}
\end{columns}

\begin{columns}
    \begin{column}{0.4\textwidth}
        \onslide<3->{
            \begin{figure}[htbp]
                \centering
                \includegraphics[width=0.8\linewidth]{figures/classification_metrics/sample_data_circles_region_2.pdf}
            \end{figure}
        }
    \end{column}
    \begin{column}{0.6\textwidth}
        \onslide<4->{
            \begin{equation*}
                E = 1 - \max \cbrackets{\frac{4}{8}, \frac{4}{8}} =  1 - \frac{4}{8} = \frac{1}{2}
            \end{equation*}
        }
    \end{column}
\end{columns}

\begin{columns}
    \begin{column}{0.4\textwidth}
        \onslide<5->{
            \begin{figure}[htbp]
                \centering
                \includegraphics[width=0.8\linewidth]{figures/classification_metrics/sample_data_circles_region_3.pdf}
            \end{figure}
        }
    \end{column}
    \begin{column}{0.6\textwidth}
        \onslide<6->{
            \begin{equation*}
                E = 1 - \max \cbrackets{\frac{4}{8}, \frac{2}{8}, \frac{1}{8}, \frac{1}{8}} = 1 - \frac{4}{8} = \frac{1}{2}
            \end{equation*}
        }
    \end{column}
\end{columns}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Entropy - history}

\begin{itemize}
    \item \emph{Claude Elwood Shannon} introduced this notion into the field of information theory in his work ``\emph{A Mathematical Theory of Communication}'' in 1948 \cite{Shannon48}.
\end{itemize}

\begin{columns}
    \begin{column}{0.35\textwidth}
        \begin{figure}[htbp]
            \centering
            \includegraphics[width=\linewidth]{figures/classification_metrics/shannon.jpg}
        \end{figure}
    \end{column}
    \begin{column}{0.65\textwidth}
        \begin{figure}[htbp]
            \centering
            \includegraphics[width=0.9\linewidth]{figures/classification_metrics/shannon_entropy_paper.png}
        \end{figure}
    \end{column}
\end{columns}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Entropy - Dictionary Definition}

\begin{itemize}
    \item A \textbf{thermodynamic quantity} representing the unavailability of a system's thermal energy for conversion into mechanical work; \textbf{lack} of \textbf{order} or \textbf{predictability}.
    \item An \textbf{abstract term} that is broadly used across \textbf{numerous disciplines} with a \textbf{simple intuition} behind.
\end{itemize}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.7\linewidth]{figures/classification_metrics/entropy_illustration_circles.png}
\end{figure}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Entropy - Intuition}

\begin{itemize}
    \item The amount of \textbf{uncertainty}.
    \item How many ways is it possible to rearrange the sets of letters below to form new distinct permutations?
    \begin{enumerate}[<+->]
        \item \texttt{A-A-A-A-A-A}
        \item \texttt{A-A-A-A-A-B}
        \item \texttt{A-A-A-B-B-B}
    \end{enumerate}
    \item Physical problem: how fast are the particles moving?
\end{itemize}

\begin{columns}
    \begin{column}{0.33\textwidth}
        \begin{figure}[htbp]
            \centering
            \includegraphics[width=\linewidth]{figures/classification_metrics/ice.jpeg}
        \end{figure}
    \end{column}
    \begin{column}{0.33\textwidth}
        \begin{figure}[htbp]
            \centering
            \includegraphics[width=\linewidth]{figures/classification_metrics/water.jpg}
        \end{figure}
    \end{column}
    \begin{column}{0.33\textwidth}
        \begin{figure}[htbp]
            \centering
            \includegraphics[width=\linewidth]{figures/classification_metrics/vapor.jpg}
        \end{figure}
    \end{column}
\end{columns}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{[Task 1] Entropy - Game}

\begin{columns}
    \begin{column}{0.7\textwidth}
        \begin{itemize}
            \item Assume a list of letters. One letter is chosen randomly. The task is to guess the chosen letter by asking the minimum possible number of questions with a ``Yes/No'' answer.
            \item Example of questions:
            \begin{itemize}
                \item `\textit{`Is it a letter X or Y?}'' $\to$ \textit{Yes}.
                \item ``\textit{It is a letter M?}'' $\to$ \textit{No}.
            \end{itemize}
            \item What list of letters would be the easiest to win with?
            \begin{enumerate}[<+->]
                \item \texttt{A-A-A-A-A-A}
                \item \texttt{A-A-A-A-A-B}
                \item \texttt{A-A-A-B-B-B}
                \item \texttt{A-B-C-D-E-F}
            \end{enumerate}
        \end{itemize}
    \end{column}
    \begin{column}{0.3\textwidth}
        \begin{figure}[htbp]
            \centering
            \includegraphics[width=\linewidth]{figures/classification_metrics/question_mark.png}
        \end{figure}
    \end{column}
\end{columns}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Entropy - The Amount of Uncertainty}

\begin{itemize}
    \item The \textbf{higher} the \textbf{uncertainty}, the lower our \textbf{confidence} in our \textbf{predictions}.
    \item In other words, the amount of \textbf{uncertainty}/surprise/doubt increases with increasing \textbf{entropy}.
\end{itemize}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.9\linewidth]{figures/classification_metrics/entropy_low_high.png}
\end{figure}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{[Task 2] Entropy - Game}

\begin{itemize}
    \item Assume a list of letters ``\texttt{A-B-C-D}'' from which one letter is chosen \textbf{randomly} with uniform probability distribution. The task is to \textbf{guess} the chosen letter using the \textbf{minimum number} of \textbf{questions} with ``yes/no'' answer.
    \item Which strategy is \textbf{optimal}?
    \item How do you \textbf{evaluate} which strategy is \textbf{optimal}?
\end{itemize}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{[Task 3] Entropy - Game}

\begin{itemize}
    \item Assume a list of letters ``\texttt{A-A-A-A-B-B-C-D}'' from which one letter is \textbf{chosen} \textbf{randomly} with uniform probability distribution. The task is to guess the chosen letter using the \textbf{minimum number} of \textbf{questions} with ``yes/no'' answer.
    \item Which strategy is \textbf{optimal}?
    \item How do you \textbf{evaluate} which strategy is \textbf{optimal}?
\end{itemize}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Logarithm}

\begin{itemize}
    \item \textbf{Inverse} exponential function.
\end{itemize}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=\linewidth]{figures/classification_metrics/logarithm_plot.png}
\end{figure}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Important properties of logarithms}

\begin{itemize}
    \item Some \textbf{relevant properties} of logarithms:
    \begin{equation}
        \func{\log_a}{xy} = \func{\log_a}{x} + \func{\log_a}{y}
    \end{equation}
    \begin{equation}
        \func{\log_a}{x} = -\func{\log_a}{\frac{1}{x}}
    \end{equation}
    
    \item Another identities:
    \begin{equation}
        \func{\log_a}{\frac{x}{y}} = \func{\log_a}{x} - \func{\log_a}{y}
    \end{equation}
    
    \begin{equation}
        \func{\log_a}{x^y} = y \func{\log_a}{x}
    \end{equation}
    
    \begin{equation}
        \func{\log_b}{x} = \frac{\func{\log_a}{x}}{\func{\log_a}{b}}
    \end{equation}
\end{itemize}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Entropy - Mathematical Definition}

\begin{itemize}
    \item \textbf{Expected value of information}.
    \item The base-$2$ logarithm is used to obtain the result in \textbf{bits} (due to binary number system). Other bases would also work, but are more difficult to interpret.
    \item Let $X$ be a \textbf{discrete random variable} that result in one of $n$ different states. Then, the \textbf{entropy} of this random variable is defined as
    \begin{equation}
        \func{H}{X} = -\sum_{i = 1}^n \func{P}{X = x_i} \func{log_2}{\func{P}{X = x_i}}.
    \end{equation}
\end{itemize}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Entropy and Decision Trees}

\begin{itemize}
    \item We \textbf{compute the entropy} of a single class $k$ as
    \begin{equation}
        h_k = -p_k \func{log_2}{p_k}.
    \end{equation}
    
    \item The \textbf{resulting entropy} for a given region is determined by the \textbf{sum} of all \textbf{partial entropies} across all classes as follows
    \begin{equation}
        H = \sum_{k = 1}^K h_k = -\sum_{k = 1}^K p_k \func{log_2}{p_k}.
    \end{equation}
    
    \item The \textbf{objective} is to \textbf{minimize the entropy}.
\end{itemize}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Entropia - \datasetname{Iris} dataset}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.9\linewidth]{figures/classification_metrics/iris_tree_entropy.png}
\end{figure}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Entropy - Demonstration}

\scriptsize{
    \begin{columns}
        \begin{column}{0.4\textwidth}
            \onslide<1->{
                \begin{figure}[htbp]
                    \centering
                    \includegraphics[width=0.8\linewidth]{figures/classification_metrics/sample_data_circles_region_1.pdf}
                \end{figure}
            }
        \end{column}
        \begin{column}{0.6\textwidth}
            \onslide<2->{
                \begin{equation*}
                    H = -\frac{8}{8} \func{log_2}{\frac{8}{8}} = 0
                \end{equation*}
            }
        \end{column}
    \end{columns}
    
    \begin{columns}
        \begin{column}{0.4\textwidth}
            \onslide<3->{
                \begin{figure}[htbp]
                    \centering
                    \includegraphics[width=0.8\linewidth]{figures/classification_metrics/sample_data_circles_region_2.pdf}
                \end{figure}
            }
        \end{column}
        \begin{column}{0.6\textwidth}
            \onslide<4->{
                \begin{equation*}
                    H = -\frac{4}{8} \func{log_2}{\frac{4}{8}} - \frac{4}{8} \func{log_2}{\frac{4}{8}} = 1
                \end{equation*}
            }
        \end{column}
    \end{columns}
    
    \begin{columns}
        \begin{column}{0.4\textwidth}
            \onslide<5->{
                \begin{figure}[htbp]
                    \centering
                    \includegraphics[width=0.8\linewidth]{figures/classification_metrics/sample_data_circles_region_3.pdf}
                \end{figure}
            }
        \end{column}
        \begin{column}{0.6\textwidth}
            \onslide<6->{
                \begin{equation*}
                    \begin{aligned}
                        H = &-\frac{4}{8} \func{log_2}{\frac{4}{8}} -
                        \frac{2}{8} \func{log_2}{\frac{2}{8}} -\\
                        &\frac{1}{8} \func{log_2}{\frac{1}{8}} -
                        \frac{1}{8} \func{log_2}{\frac{1}{8}} =
                        \frac{7}{4} = 1.75
                    \end{aligned}
                \end{equation*}
            }
        \end{column}
    \end{columns}
}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Gini Index}

\begin{itemize}
    \item The aim is to estimate the purity of a certain set.
    \item The objective is to minimize this coefficient.
    \item The Gini Index is computer for the class $k$ as
    \begin{equation}
        g_k = p_k \rbrackets{1 - p_k}
    \end{equation}
    \item Whereas its value for all classes in a specific region is determined as
    \begin{equation}
        G = \sum_{k = 1}^K g_k = \sum_{k = 1}^K p_k \rbrackets{1 - p_k} = 1 - \sum_{k = 1}^K p^2_k
    \end{equation}
\end{itemize}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Gini Index - \datasetname{Iris} dataset}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.9\linewidth]{figures/classification_metrics/iris_tree_gini.png}
\end{figure}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Gini Index - Demonstration}

\scriptsize{
    \begin{columns}
        \begin{column}{0.4\textwidth}
            \onslide<1->{
                \begin{figure}[htbp]
                    \centering
                    \includegraphics[width=0.8\linewidth]{figures/classification_metrics/sample_data_circles_region_1.pdf}
                \end{figure}
            }
        \end{column}
        \begin{column}{0.6\textwidth}
            \onslide<2->{
                \begin{equation*}
                    G = \frac{8}{8} \rbrackets{1 - \frac{8}{8}} = 0
                \end{equation*}
            }
        \end{column}
    \end{columns}
    
    \begin{columns}
        \begin{column}{0.4\textwidth}
            \onslide<3->{
                \begin{figure}[htbp]
                    \centering
                    \includegraphics[width=0.8\linewidth]{figures/classification_metrics/sample_data_circles_region_2.pdf}
                \end{figure}
            }
        \end{column}
        \begin{column}{0.6\textwidth}
            \onslide<4->{
                \begin{equation*}
                    G = \frac{4}{8} \rbrackets{1 - \frac{4}{8}} + \frac{4}{8} \rbrackets{1 - \frac{4}{8}} = \frac{2}{4} = 0.5
                \end{equation*}
            }
        \end{column}
    \end{columns}
    
    \begin{columns}
        \begin{column}{0.4\textwidth}
            \onslide<5->{
                \begin{figure}[htbp]
                    \centering
                    \includegraphics[width=0.8\linewidth]{figures/classification_metrics/sample_data_circles_region_3.pdf}
                \end{figure}
            }
        \end{column}
        \begin{column}{0.6\textwidth}
            \onslide<6->{
                \begin{equation*}
                    \begin{aligned}
                        H = &\frac{4}{8} \rbrackets{1 - \frac{4}{8}} +
                        \frac{2}{8} \rbrackets{1 - \frac{2}{8}} +\\
                        &\frac{1}{8} \rbrackets{1 - \frac{1}{8}} -
                        \frac{1}{8} \rbrackets{1 - \frac{1}{8}} =
                        \frac{21}{32} = 0.65625
                    \end{aligned}
                \end{equation*}
            }
        \end{column}
    \end{columns}
}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Gini Index vs. Entropia vs. Classification Error}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=\linewidth]{figures/classification_metrics/gini_entropy_mis_err.png}
\end{figure}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Information gain (IG)}

\begin{itemize}
    \item Measures the amount of information a specific predictor provides about the predicted class given the current separation of the decision region.
    \item When building the decision tree, the endeavor is to maximize IG.
    \item The attribute with maximum IG is to be used to separate the region.
    \item Conceptually speaking, we compute it as
    \begin{equation}
        \begin{aligned}
            IG = &\rbrackets{\text{entropy of the parent)}} -\\
            &\rbrackets{\text{weighted average of the entropies of the children)}}.
        \end{aligned}
    \end{equation}
\end{itemize}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Information gain (IG)}

\begin{itemize}
    \item Let $\func{I}{D}$ be a function that measures \textbf{the amount of information} (e.g., entropy, Gini index, ...).
    \item Let $N_{left}$ and  $N_{right}$ be the number of element in the left and right subtree, respectively.
    \item Let $D_{left}$ and $D_{right}$ be proportions of data that belong to the left and right subtree, respectively. Let $N_p$ denote the number of elements in the current node, such that $N_p = N_{left} + N_{right}$.
    \item Therefore, IG is determined using the weighted average like this:
    \begin{equation}
        \func{IG}{D_p} =
        \func{I}{D_p} -
        \rbrackets{
            \frac{N_{left}}{N_p} \func{I}{D_{left}} +
            \frac{N_{right}}{N_p} \func{I}{D_{right}}
        }.
    \end{equation}
\end{itemize}

\end{frame}
% --------------------------------------------------------------------------------------------------