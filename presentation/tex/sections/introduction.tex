% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Motivation}

\begin{itemize}
    \item We tackle the problem of \textbf{class/category prediction} based on input data.
    \item We will use the term \textbf{class} in this presentation.
    \item The class belongs to a \textbf{finite set of values}.
    \item Example: the class may represent a diagnosis (malignant/bening) based on tissue measurements of some patient.
    \item The process of determining the class is called \textbf{classification}.
\end{itemize}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Classification Task}

\begin{itemize}
    \item \textbf{Common problem} in the field of \textbf{machine learning}.
    \item The goal is to \textbf{predict} a \textbf{class}.
    \item \textbf{Finding a proper decision boundary} in an $N$-dimensional space.
\end{itemize}

\begin{center}
    \begin{tabular}{c}
        \includegraphics[scale=0.15]{figures/introduction/binary_classification.png}\\
    \end{tabular}
\end{center}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Data Set}

\begin{itemize}
    \item Its shorter form ``\textbf{dataset}'' is used more often.
    \item A set of data used to \textbf{create} (\textbf{train}) and \textbf{test} the model.
    \item We assume the dataset is a \textbf{table} consisting of \textbf{rows} and \textbf{columns}.
    \item The \textbf{columns} are often called \textbf{variables} or \textbf{features}.
    \item We will refer to columns as \textbf{features}.
\end{itemize}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Variables}

\begin{itemize}
    \item \textbf{Quantitative}
    \begin{itemize}
        \item Characterized by a \textbf{number}.
        \begin{itemize}
            \item Discrete (integer).
            \item Continuous (real number).
        \item \textbf{Regression} problem.
        \item Example: age, income, coordinate, ...
        \end{itemize}
    \end{itemize}
    
    \item \textbf{Qualitative}
    \begin{itemize}
        \item Characterized by a \textbf{class}.
        \item \textbf{Classification} problem.
        \item Example: gender (man/woman), diagnosis (malignant/benign; diabetes - yes/no), ...
    \end{itemize}
\end{itemize}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Variables}

\begin{itemize}
    \item Also known as \textbf{predictors}.
    \begin{itemize}
        \item \textbf{Features} (the most frequently used term).
        \item Explanatory variables.
        \item Independent variables.
        \item Input variables.
        \item Attributes.
        \item Typically represented by $\mtx{X}$.
    \end{itemize}
    
    \item \textbf{Predicted values}.
    \begin{itemize}
        \item \textbf{Labels} (the most frequently used term).
        \item Output variables.
        \item Dependent variables.
        \item Response variables.
        \item Typically represented by $\mtx{Y}$.
    \end{itemize}
\end{itemize}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Dataset \datasetname{Iris}}

\begin{itemize}
    \item An example of a well-known dataset \datasetname{Iris} (a certain flower).
    \item Three different types of the iris \externalsrc{\cite{irisdatasetwiki}}.
\end{itemize}

\begin{columns}
    \begin{column}{0.33\textwidth}
        \begin{figure}[htbp]
            \centering
            \includegraphics[width=\linewidth]{figures/introduction/iris_setosa.jpg}
            \caption{Iris Setosa.}
            \label{fig:IrisSetosa}
        \end{figure}
    \end{column}
    \begin{column}{0.33\textwidth}
        \begin{figure}[htbp]
            \centering
            \includegraphics[width=\linewidth]{figures/introduction/iris_versicolor.jpg}
            \caption{Iris Versicolor.}
            \label{fig:IrisVersicolor}
        \end{figure}
    \end{column}
    \begin{column}{0.33\textwidth}
        \begin{figure}[htbp]
            \centering
            \includegraphics[width=\linewidth]{figures/introduction/iris_virginica.jpg}
            \caption{Iris Virginica.}
            \label{fig:IrisVirginica}
        \end{figure}
    \end{column}
\end{columns}

\end{frame}
% --------------------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------------------
\begin{frame}[fragile]{Dataset \datasetname{Iris}}

\footnotesize{
    \begin{table}[htbp]
        \centering
        \begin{tabular}{|r|r|r|r|r|}
            \toprule
            \tblcolname{sepal length} &
            \tblcolname{sepal width} &
            \tblcolname{petal length} &
            \tblcolname{petal width} &
            \tblcolname{species}\\
            \midrule
                5.5 & 2.3 & 4.0 & 1.3 & 1.0\\
                \hline
                6.0 & 3.0 & 4.8 & 1.8 & 2.0\\
                \hline
                6.3 & 3.3 & 4.7 & 1.6 & 1.0\\
                \hline
                6.9 & 3.2 & 5.7 & 2.3 & 2.0\\
                \hline
                6.2 & 3.4 & 5.4 & 2.3 & 2.0\\
                \hline
                5.1 & 3.4 & 1.5 & 0.2 & 0.0\\
                \hline
                6.1 & 2.8 & 4.7 & 1.2 & 1.0\\
                \hline
                4.6 & 3.4 & 1.4 & 0.3 & 0.0\\
            \bottomrule
        \end{tabular}
        
        \caption{A random sample from the \datasetname{Iris} dataset.}
        \label{tab:IrisDatasetPreview}
    \end{table}
}

\end{frame}
% --------------------------------------------------------------------------------------------------
