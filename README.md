# MLSS 2021

**Machine Learning Summer School** 2021 at **University of Žilina** (Slovakia, Europe).

This repository contains study materials for the **decision trees** section taught by *Milan Ondrašovič*.
